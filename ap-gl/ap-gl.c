/*
 *      ap-gl.c from Wireless Access Point Utilites for Unix
 *
 * Copyright (c) 2002 Roman Festchook <roma at polesye dot net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2 from
 * June 1991 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

#include <stdlib.h>
#include <string.h>
#include <menu.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include "ap-utils.h"

WINDOW *win_for_menu, *sub_for_menu, *win_for_help, *main_win, *main_sub;
char *community, *name;
int atmel410_filter = 1, wait_mode = WAIT_FOREVER;
struct in_addr ap_ip;
char *prog_title = "Wireless AP Configurator for GlobalSun";
char set_oeminfo_allowed = 0;
extern char *ap_types[];

void config_menu()
{
    struct umitems umenu_atmel[] = {
	{_("Bridging"), _("Set bridging and IP-related options"), bridging,
	 0},
	{_("Wireless"), _("Set wireless options"), atmel_wireless, 0},
	{_("Privacy"), MENU_ENCRYPT, wep, 0},
	{_("Auth"), MENU_AUTH, atmel_auth, 0},
	{_("Community"), MENU_COMMUNITY, AuthorizedSettings, 0},
	{_("Radio"), _("Set radio signal power and antenna options"),
	 power, 0},
	{"..", MAIN_MENU, NULL, 0},
	{0, 0, NULL, 0}
    };

   set_oeminfo_allowed++;
   uni_menu(umenu_atmel, sizeof(umenu_atmel) / sizeof(umenu_atmel[0]));
   set_oeminfo_allowed--;
}


void command_menu()
{
    struct umitems command_umenu_atmel[] = {
	{_("Upload"), _("Activate current configuration"), upload, 0},
	{_("Defaults"), _("Restore factory default settings"), defaults, 0},
	{_("Reset"),
	 _("Reset AP. All not uploaded configuration will be lost"), reset, 0},
	{_("TestMode"), _("Put Access Point in test mode"), test, 0},
	{"..", MAIN_MENU, NULL, 0},
	{0, 0, NULL, 0}
    };

    ap_types[1] = "ATMEL_PRISM";

    uni_menu(command_umenu_atmel, sizeof(command_umenu_atmel) / sizeof(command_umenu_atmel[0]));
}

void stat_menu()
{
    struct umitems umenu_atmel[] = {
	{_("SysInfo"), MENU_SYSINFO, atmel_sysinfo, 0},
	{_("Ethernet"), _("Get ethernet port statistics"), EthStat, 0},
	{_("Wireless"), MENU_WIRELESS, WirelessStat, 0},
	{_("Stations"), MENU_STAS, atmel_stations, 0},
	{_("KnownAPs"), _("Get info about known Access Points"), APs, 0},
	{"..", MAIN_MENU, NULL, 0},
	{0, 0, NULL, 0}
    };
    
    uni_menu(umenu_atmel, sizeof(umenu_atmel) / sizeof(umenu_atmel[0]));
}

void _auth()
{
    if (get_opts() == 0)
	connect_options((unsigned long) NULL, ATMEL410+1);
}

void main_menu()
{
    struct umitems config_umenu[] = {
	{_("Info"), MENU_INFO, stat_menu, 1},
	{_("Config"), MENU_CONFIG, config_menu, 1},
	{_("Commands"), _("Execute commands on Access Point"),
	 command_menu, 1},
	{_("Connect"), MENU_CONNECT, _auth, 0},
	{_("Search"), MENU_SEARCH, ap_search, 0},
	{_("About"), MENU_ABOUT, about, 0},
	{_("Shell"), MENU_SHELL, exit_shell, 0},
	{_("Exit"), MENU_EXIT, exit_program, 0},
	{0, 0, NULL, 0}
    };
    uni_menu(config_umenu, sizeof(config_umenu) / sizeof(config_umenu[0]));
}



int main( /*int argc, char **argv */ )
{
    int i;
    WINDOW *win_for_title;
    char message[100];

    ap_types[0]="ATMEL_PRISM";

#ifdef HAVE_GETTEXT
    /* locale support init */
    setlocale(LC_ALL, "");
    bindtextdomain("ap-utils", LOCALEDIR);
    textdomain("ap-utils");
#endif

    initscr();
    if (has_colors()) {
	start_color();

	/* Never trust that these are defined by default. */
	init_pair (COLOR_RED, COLOR_RED, COLOR_BLACK );
	init_pair (COLOR_GREEN, COLOR_GREEN, COLOR_BLACK);
	init_pair (COLOR_YELLOW, COLOR_YELLOW, COLOR_BLACK);
	init_pair (COLOR_BLUE, COLOR_BLUE, COLOR_BLACK);
	init_pair (COLOR_MAGENTA, COLOR_MAGENTA, COLOR_BLACK );
	init_pair (COLOR_CYAN, COLOR_CYAN, COLOR_BLACK);

	init_pair(11, COLOR_BLACK, COLOR_CYAN);
	init_pair(12, COLOR_BLACK, COLOR_WHITE);
	init_pair(13, COLOR_BLACK, COLOR_GREEN);
	init_pair(14, COLOR_WHITE, COLOR_RED);
    }

    noraw();
    cbreak();
    noecho();
    scrollok(stdscr, TRUE);
    idlok(stdscr, TRUE);
    keypad(stdscr, TRUE);
    refresh();

/* draw help win */
    win_for_help = newwin(1, COLS, LINES - 1, 0);
    wattrset(win_for_help, COLOR_PAIR(11));
    print_help("");

/* draw title win */
    win_for_title = newwin(1, COLS, 0, 0);
    wattrset(win_for_title, COLOR_PAIR(11));
    for (i = 0; i < COLS; i++)
	waddch(win_for_title, ' ');
    sprintf(message, _("Wireless Access Point Configurator ver. %s"),
	    VERSION);
    mvwaddstr(win_for_title, 0, (COLS - strlen(message)) / 2, message);
    wrefresh(win_for_title);

/* draw menu win */
    win_for_menu = newwin(LINES - 2, MCOLS, 1, 0);
    sub_for_menu = derwin(win_for_menu, LINES - 5, MCOLS - 2, 2, 1);
    set_menu_win(NULL, win_for_menu);
    set_menu_sub(NULL, sub_for_menu);
    attrset(COLOR_PAIR(11));

    /* ����� ����� */
    waddch(win_for_menu, ACS_BSSB);
    for (i = 0; i < MCOLS - 2; i++)
	waddch(win_for_menu, ACS_BSBS);
    waddch(win_for_menu, ACS_BSSS);
    for (i = 0; i < LINES - 4; i++) {
	waddch(win_for_menu, ACS_SBSB);
	mvwaddch(win_for_menu, i + 1, MCOLS - 1, ACS_SBSB);
    }
    waddch(win_for_menu, ACS_SSBB);
    for (i = 0; i < MCOLS - 2; i++)
	waddch(win_for_menu, ACS_BSBS);
    waddch(win_for_menu, ACS_SSBS);
    wrefresh(win_for_menu);

/* draw_main_win */
    main_win = newwin(LINES - 2, COLS - MCOLS, 1, MCOLS);
    /* ����� ����� */
    for (i = 0; i < COLS - MCOLS - 1; i++)
	waddch(main_win, ACS_BSBS);
    waddch(main_win, ACS_BBSS);
    for (i = 0; i < LINES - 4; i++)
	mvwaddch(main_win, i + 1, COLS - MCOLS - 1, ACS_SBSB);
    for (i = 0; i < COLS - MCOLS - 1; i++)
	waddch(main_win, ACS_BSBS);
    waddch(main_win, ACS_SBBS);
    main_sub = derwin(main_win, LINES - 4, COLS - MCOLS - 1, 1, 0);
    wclear(main_sub);
    wrefresh(main_win);

    about();
    if (get_opts() == 0)
	connect_options((unsigned long) NULL, ATMEL410+1);

    while (1)
	main_menu();

/* Not reachable */
    return 0;
}

