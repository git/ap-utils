Summary:	Configure and monitor Wireless Access Points 
Name:		ap-utils
Version:	1.3.4pre1
Release:	0

Source:		%name-%version.tar.bz2

License:	GPL
Group:		Networking/Other
URL:		http://ap-utils.polesye.net/
Buildroot:	%_tmppath/%name-buildroot

%description
Wireless Access Point Utilities for Unix is a set of utilities 
to configure and monitor Wireless Access Points under Unix.

%prep
rm -rf $RPM_BUILD_ROOT

%setup -q

%build

%configure

%make

%install

%makeinstall

%find_lang %name

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %name.lang
%defattr (-,root,root)
%doc AUTHORS INSTALL NEWS COPYING README TODO ChangeLog
%doc Documentation/FAQ Documentation/*.html 
%_bindir/*
%_sbindir/*
%_mandir/man8/*

%changelog
* Mon Nov 24 2003 Jacek Pliszka <pliszka@fuw.edu.pl> 1.3.4-pre1
- 1.3.4-pre1

* Mon Oct 27 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.3.3-1mdk
- 1.3.3

* Sat Jul 26 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.3.2-1mdk
- 1.3.2

* Sat Apr 25 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.3.1-2mdk
- adjust buildrequires

* Fri Mar 21 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.3.1-1mdk
- 1.3.1

* Sat Feb 01 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.3-2mdk
- rebuild

* Fri Dec 06 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.3-1mdk
- 1.3
- add locales & man pages

* Tue Nov 12 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.2-1mdk
- 1.2

* Fri Oct 04 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.1.1-1mdk
- 1.1.1

* Tue Jul 16 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.3-1mdk
- 1.0.3

* Fri Jul 06 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.0.2-1mdk
- 1.0.2
