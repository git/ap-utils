/*
 *      auth.c from Access Point SNMP Utils for Linux
 *
 * Copyright (c) 2002 Roman Festchook <roma at polesye dot net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2 from
 * June 1991 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include "ap-utils.h"

#define COMMUNITY_TOP_HEADER _("Set community/password")
#define COMMUNITY_HEADER1 _("Key Access level")
#define COMMUNITY_HEADER2 _("Community/Password")
#define COMMUNITY_USER  _("[U] User ")
#define COMMUNITY_ADMIN  _("[A] Administrator ")
#define COMMUNITY_MANUF  _("[M] Manufacturer ")
#define COMMUNITY_HELP _("[key] - set community/password; W - write config to AP; Q - quit to menu")

extern WINDOW *main_sub;
extern short ap_type;

void AuthorizedSettings()
{
    char AuthorizedUserPass[3][12] = {
	{0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x09, 0x01,
	 0x00},
	{0x2B, 0x06, 0x01, 0x04, 0x01, 0x87, 0x29, 0x02, 0x01, 0x02, 0x01,
	 0x00},
       {0x2B, 0x06, 0x01, 0x04, 0x01, 0xE0, 0x3E, 0x01, 0x01, 0x09, 0x01,
	 0x00}
    };
    char AuthorizedAdminPass[3][12] = {
	{0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x09, 0x02,
	 0x00},
	{0x2B, 0x06, 0x01, 0x04, 0x01, 0x87, 0x29, 0x02, 0x01, 0x02, 0x02,
	 0x00},
        {0x2B, 0x06, 0x01, 0x04, 0x01, 0xE0, 0x3E, 0x01, 0x01, 0x09, 0x02,
         0x00}
    };
    char AuthorizedManufactPass[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x09, 0x03,
	0x00
    };

    varbind varbinds[3];
    char user[16], admin[16], manufact[16];
    int i;

    if (ap_type == ATMEL12350) {
	AuthorizedManufactPass[5] = 0xE0;
	AuthorizedManufactPass[6] = 0x3E;
    }

    user[0] = admin[0] = manufact[0] = '\0';
    print_top(NULL, COMMUNITY_TOP_HEADER);

    mvwaddstr(main_sub, 0, 0, COMMUNITY_HEADER1);
    mvwaddstr(main_sub, 0, 37, COMMUNITY_HEADER2);
    mvwaddstr(main_sub, 1, 0, COMMUNITY_USER);
    mvwaddstr(main_sub, 2, 0, COMMUNITY_ADMIN);

    if (ap_type == ATMEL410 || ap_type == ATMEL12350) {
	mvwaddstr(main_sub, 3, 0, COMMUNITY_MANUF);
    }
    wrefresh(main_sub);

    noecho();
    print_help(COMMUNITY_HELP);
    while (1) {
	switch (getch()) {
	default:
	    continue;
	case 'U':
	case 'u':
/*	    get_value(user, 1, 37, sizeof(user), ANY_STRING, 0, 0, NULL); */
	    get_pass(user, 1, 37, sizeof(user));
	    continue;
	case 'A':
	case 'a':
/*	    get_value(admin, 2, 37, sizeof(admin), ANY_STRING, 0, 0, NULL); */
	    get_pass(admin, 2, 37, sizeof(admin));
	    continue;
	case 'M':
	case 'm':
	    if (ap_type == ATMEL410 || ap_type == ATMEL12350)
/*
		get_value(manufact, 3, 37, sizeof(manufact), ANY_STRING,
		    0, 0, NULL);
*/
		get_pass(manufact, 3, 37, sizeof(manufact));
	    continue;
	case 'Q':
	case 'q':
	    goto quit;
	case 'W':
	case 'w':
	    i = 0;
	    if (strlen(user)) {
		varbinds[i].oid = AuthorizedUserPass[ap_type];
		varbinds[i].len_oid = sizeof(AuthorizedUserPass[ap_type]);
		varbinds[i].value = user;
		varbinds[i].len_val = strlen(user);
		varbinds[i].type = STRING_VALUE;
		i++;
	    }
	    if (strlen(admin)) {
		varbinds[i].oid = AuthorizedAdminPass[ap_type];
		varbinds[i].len_oid = sizeof(AuthorizedAdminPass[ap_type]);
		varbinds[i].value = admin;
		varbinds[i].len_val = strlen(admin);
		varbinds[i].type = STRING_VALUE;
		i++;
	    }
	    if ((ap_type == ATMEL410 || ap_type == ATMEL12350) && strlen(manufact)) {
		varbinds[i].oid = AuthorizedManufactPass;
		varbinds[i].len_oid = sizeof(AuthorizedManufactPass);
		varbinds[i].value = manufact;
		varbinds[i].len_val = strlen(manufact);
		varbinds[i].type = STRING_VALUE;
		i++;
	    }
	    print_help(WAIT_SET);
	    if (snmp(varbinds, i, SET) <= 0) {
		print_helperr(ERR_SET);
		goto exit;
	    }
	    wbkgd(main_sub, A_NORMAL);
	    wrefresh(main_sub);
	    break;
	}
	break;
    }

    print_help(DONE_SET);
  exit:
    getch();
  quit:
    print_help("");
    print_top(NULL, NULL);
    clear_main(0);
}
