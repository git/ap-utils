/*
 *      wep.c from Access Point SNMP Utils for Linux
 *
 * Copyright (c) 2002 Roman Festchook <roma at polesye dot net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2 from
 * June 1991 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include "ap-utils.h"

#define PRIVSETTINGS _("Privacy Settings")
#define ENCRYPT _("[E] Standard encryption mechanism: ")
#define ALLOW _("[A] Allow unencrypted: ")
#define DEFAULT _("[K] Default WEP key: ")
#define PUBLIC _("[P] Public key: ")

extern WINDOW *main_sub;
extern short ap_type;

void wep()
{
    char defaultWEPKey[4][12] = {
	{0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x02, 0x02, 0x01, 0x00},
	{0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x02, 0x02, 0x02, 0x00},
	{0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x02, 0x02, 0x03, 0x00},
	{0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x02, 0x02, 0x04, 0x00}
    };
    char privacyWEPEnable[] =
	{0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x02, 0x02, 0x05, 0x00
    };
    char privacyDefaultWEPKeyID[] =
	{0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x02, 0x02, 0x06, 0x00
    };

    char message[1024], m_wep = 0, m_wepid = 0,	wepkey[4][13],
	m_wepkey[4] = { 0, 0, 0, 0 };
    char *weps[3] = { "WEP64", OFF, "WEP128" }, *keys[4] = {"1", "2", "3", "4"};
    int wep, wepid, i, c = 0;
    varbind varbinds[6];
    unsigned int wepid_hw, wep_hw;

    if (ap_type == ATMEL12350) {
	privacyDefaultWEPKeyID[5] = 0xE0;
	privacyDefaultWEPKeyID[6] = 0x3E;
	privacyWEPEnable[5] = 0xE0;
	privacyWEPEnable[6] = 0x3E;
	for (i = 0; i < 4; i++) {
	    defaultWEPKey[i][5] = 0xE0;
	    defaultWEPKey[i][6] = 0x3E;
	}
    }

    varbinds[0].oid = privacyWEPEnable;
    varbinds[0].len_oid = sizeof(privacyWEPEnable);
    varbinds[0].type = NULL_VALUE;
    varbinds[0].len_val = 0;
    varbinds[1].oid = privacyDefaultWEPKeyID;
    varbinds[1].len_oid = sizeof(privacyDefaultWEPKeyID);
    varbinds[1].type = NULL_VALUE;
    varbinds[1].len_val = 0;
    print_help(WAIT_RET);
    if (snmp(varbinds, 2, GET) < 2) {
	print_helperr(ERR_RET);
	goto exit;
    }
    wep = *(varbinds[0].value);
    wepid = *(varbinds[1].value);
    print_help(_("EK1234 - set; W - write conf; Q - quit to menu"));

    print_top(NULL, PRIVSETTINGS);

    sprintf(message, "%s%s", ENCRYPT, weps[wep - 1]);
    mvwaddstr(main_sub, 0, 0, message);
    sprintf(message, "%s%d", DEFAULT, wepid);
    mvwaddstr(main_sub, 1, 0, message);

    mvwaddstr(main_sub, 3, 2, _("Key  WEP"));

    for (i = 0; i < 4; i++) {
	sprintf(message, "[%d]  00.00.00.00.00.00.00.00.00.00.00.00.00",
		i + 1);
	mvwaddstr(main_sub, i + 4, 2, message);
    }
    mvwaddstr(main_sub, LAST_ROW, 1, _("Hint! Confused by WEP key values? See man ap-config for info..."));

    wrefresh(main_sub);
    noecho();

    while (1) {
	c = getch();
	switch (c) {
	case 'q':
	case 'Q':
	    goto quit;
	case 'e':
	case 'E':
	    wep = menu_choose(0, strlen(ENCRYPT), weps, 3) + 1;
	    clear_main_new(0, 1);
	    print_menusel(0, 0, ENCRYPT, weps[wep - 1]);
	    m_wep = 1;
	    continue;
	case 'k':
	case 'K':
	    wepid = menu_choose(1, strlen(DEFAULT), keys, 4) + 1;
	    clear_main_new(1, 2);
	    sprintf(message, "%d", wepid);
	    print_menusel(1, 0, DEFAULT, message);
	    wrefresh(main_sub);
	    m_wepid = 1;
	    continue;
	case '1':
	case '2':
	case '3':
	case '4':
	    c = c -'0';
/*
	    message[1] = '\0';
	    c = strtol(message, NULL, 16);
	    for (i = 0; i < 13; i++)
*/
		for (i = 0; i < 13; i++) {
		    get_value(message, c + 3, 7 + i * 3, 3, HEX_STRING,
			0, 0, NULL);
		    wepkey[c - 1][i] = strtol(message, NULL, 16);
		}
	    m_wepkey[c - 1] = 1;
	    continue;
	case 'w':
	case 'W':
	    i = 0;
	    if (m_wep) {
		wep_hw = swap4(wep);
		varbinds[i].oid = privacyWEPEnable;
		varbinds[i].len_oid = sizeof(privacyWEPEnable);
		varbinds[i].type = INT_VALUE;
		varbinds[i].value = (char *) &wep_hw;
		varbinds[i].len_val = 1;
		i++;
	    }
	    if (m_wepid) {
		wepid_hw = swap4(wepid);
		varbinds[i].oid = privacyDefaultWEPKeyID;
		varbinds[i].len_oid = sizeof(privacyDefaultWEPKeyID);
		varbinds[i].type = INT_VALUE;
		varbinds[i].value = (char *) &wepid_hw;
		varbinds[i].len_val = 1;
		i++;
	    }
	    for (c = 0; c < 4; c++)
		if (m_wepkey[c]) {
		    varbinds[i].oid = defaultWEPKey[c];
		    varbinds[i].len_oid = sizeof(defaultWEPKey[c]);
		    varbinds[i].type = STRING_VALUE;
		    varbinds[i].value = wepkey[c];
		    varbinds[i].len_val = 13;
		    i++;
		}
	    print_help(WAIT_SET);
	    if (snmp(varbinds, i, SET) <= 0) {
		print_helperr(ERR_SET);
		goto exit;
	    }
	    print_help(DONE_SET);
	    goto exit;
	}
	continue;
    }

  exit:
    getch();
  quit:
    print_top(NULL, NULL);
    clear_main(0);
}


void nwn_wep()
{

    char defaultWEPKey[4][11] = {	
        { 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x03, 0x01, 0x02, 0x01, 0x01},
        { 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x03, 0x01, 0x02, 0x01, 0x02},
        { 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x03, 0x01, 0x02, 0x01, 0x03},
	{ 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x03, 0x01, 0x02, 0x01, 0x04},
};

    char privacyWEPEnable[] =
	{ 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x05, 0x01, 0x01, 0x01 };
    char privacyDefaultWEPKeyID[] =
	{ 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x05, 0x01, 0x02, 0x01 };
    
    char oid_dot11ExcludeUnencrypted[] =
	{ 0x2a, 0x86, 0x48, 0xce, 0x34, 0x01, 0x05, 0x01, 0x04, 0x01 };
    char oid_smtPublicKeyEnable[] =
	{ 0x2b, 0x06, 0x01, 0x04, 0x01, 0x87, 0x29, 0x03, 0x01, 0x02, 0x01,
	0x0a, 0x00 };

    char message[1024], m_wep = 0, m_wepid = 0, wepkey[4][5],
            m_wepkey[4] = { 0, 0, 0, 0 }, exclude, m_exclude = 0;
    char *weps[2] = { "WEP64", OFF }, publickey, m_publickey = 0, *keys[4] = {"1", "2", "3", "4"}, wepid=0, wepid_hw;
    int wep, i, c=0;
    varbind varbinds[15];

    varbinds[0].oid = privacyWEPEnable;
    varbinds[0].len_oid = sizeof(privacyWEPEnable);
    varbinds[0].type = NULL_VALUE;
    varbinds[0].len_val = 0;
    varbinds[1].oid = oid_smtPublicKeyEnable;
    varbinds[1].len_oid = sizeof(oid_smtPublicKeyEnable);
    varbinds[1].type = NULL_VALUE;
    varbinds[1].len_val = 0;
    varbinds[2].oid = oid_dot11ExcludeUnencrypted;
    varbinds[2].len_oid = sizeof(oid_dot11ExcludeUnencrypted);
    varbinds[2].type = NULL_VALUE;
    varbinds[2].len_val = 0;
    varbinds[3].oid = privacyDefaultWEPKeyID;
    varbinds[3].len_oid = sizeof(privacyDefaultWEPKeyID);
    varbinds[3].type = NULL_VALUE;
    varbinds[3].len_val = 0;
			
    print_help(WAIT_RET);
    if (snmp(varbinds, 4, GET) < 4) {
	print_helperr(ERR_RET);
	goto exit;
    }
    wep = *(varbinds[0].value);
    exclude = *(varbinds[2].value);
    publickey = *(varbinds[1].value);
    wepid=*(varbinds[3].value);
    print_help(_("AEPK1234 - set options; W - write conf; Q - quit to menu"));

    print_top(NULL, PRIVSETTINGS);

    sprintf(message, "%s%s", ENCRYPT, weps[wep - 1]);
    mvwaddstr(main_sub, 1, 0, message);
    sprintf(message, "%s%s", ALLOW, (exclude == 2) ? ON : OFF);
    mvwaddstr(main_sub, 2, 0, message);
   
    sprintf(message, "%s%d", DEFAULT, wepid+1);
    mvwaddstr(main_sub, 3, 0, message);

	
    sprintf(message, "%s%s", PUBLIC, (publickey == 1) ? ON : OFF);
    mvwaddstr(main_sub, 10, 0, message);

    mvwaddstr(main_sub, 4, 2, _("Key  WEP"));
    for (i = 0; i < 4; i++) {
        sprintf(message, "[%d]  00.00.00.00.00", i + 1);
       mvwaddstr(main_sub, i + 5, 2, message);
   }


    wrefresh(main_sub);
    noecho();

    while (1) {
	switch (c = getch()) {
	case 'Q':
	case 'q':
	    goto quit;
	case 'E':
	case 'e':
	    wep = menu_choose(1, strlen(ENCRYPT), weps, 2) + 1;
	    clear_main_new(1, 2);
	    print_menusel(1, 0, ENCRYPT, weps[wep - 1]);
	    m_wep = 1;
	    continue;
	case 'a':
	case 'A':
	    exclude = on_off(2, strlen(ALLOW));
	    exclude = (exclude == 2) ? 1 : 2;
	    clear_main_new(2, 3);
	    print_menusel(2, 0, ALLOW, (exclude == 2) ? ON : OFF);
	    m_exclude = 1;
	    continue;
	case 'p':
	case 'P':
	    publickey = on_off(10, strlen(PUBLIC));
	    clear_main_new(10, 11);
	    print_menusel(10, 0, PUBLIC, (publickey == 1) ? ON : OFF);
	    m_publickey = 1;
	    continue;
        case 'k':
        case 'K':
            wepid = menu_choose(3, strlen(DEFAULT), keys, 4);
            clear_main_new(3, 4);
            sprintf(message, "%d", wepid+1);
            print_menusel(3, 0, DEFAULT, message);
            m_wepid = 1;
            continue;
        case '1':
        case '2':
        case '3':
        case '4':
//            message[0] = c;
  //          message[1] = '\0';
//            c = strtol(message, NULL, 16);
	    c = c - '0';
/*          for (i = 0; i < 5; i++)*/
                for (i = 0; i < 5; i++) {
                    get_value(message, c + 4, 7 + i * 3, 3, HEX_STRING,
			0, 0, NULL);
                    wepkey[c - 1][i] = strtol(message, NULL, 16);
                 }
            m_wepkey[c - 1] = 1;
            continue;
/*	    
	case '1':
	    for (i = 0; i < 5; i++) {
		get_value(message, 5, 7 + i * 3, 3, HEX_STRING, 0, 0, NULL);
		wepkey[i] = strtol(message, NULL, 16);
	    }
	    m_wepkey = 1;
	    continue;
*/	case 'w':
	case 'W':
	    i = 0;
	    if (m_wep) {
		varbinds[i].oid = privacyWEPEnable;
		varbinds[i].len_oid = sizeof(privacyWEPEnable);
		varbinds[i].type = 0x02;
		varbinds[i].value = (char *) &wep;
		varbinds[i].len_val = 1;
		i++;
	    }

/*	    m_wep = 0;
	    varbinds[i].oid = privacyDefaultWEPKeyID;
	    varbinds[i].len_oid = sizeof(privacyDefaultWEPKeyID);
	    varbinds[i].type = INT_VALUE;
	    varbinds[i].value = &m_wep;
	    varbinds[i].len_val = 1;
	    i++;
*/
	    if (m_exclude) {
		varbinds[i].oid = oid_dot11ExcludeUnencrypted;
		varbinds[i].len_oid = sizeof(oid_dot11ExcludeUnencrypted);
		varbinds[i].type = 0x02;
		varbinds[i].value = &exclude;
		varbinds[i].len_val = 1;
		i++;
	    }
	    if (m_publickey) {
		varbinds[i].oid = oid_smtPublicKeyEnable;
		varbinds[i].len_oid = sizeof(oid_smtPublicKeyEnable);
		varbinds[i].type = 0x02;
		varbinds[i].value = &publickey;
		varbinds[i].len_val = 1;
		i++;
	    }
/*	    if (m_wepkey) {
		varbinds[i].oid = defaultWEPKey;
		varbinds[i].len_oid = sizeof(defaultWEPKey);
		varbinds[i].type = STRING_VALUE;
		varbinds[i].value = wepkey;
		varbinds[i].len_val = 5;
		i++;
	    }
*/
           if (m_wepid) {
	     wepid_hw = swap4(wepid);
	     varbinds[i].oid = privacyDefaultWEPKeyID;
	     varbinds[i].len_oid = sizeof(privacyDefaultWEPKeyID);
	     varbinds[i].type = INT_VALUE;
	     varbinds[i].value = (char *) &wepid_hw;
	     varbinds[i].len_val = 1;
	     i++;
	   }
	    

		    print_help(WAIT_SET);
		if (snmp(varbinds, i, SET) <= 0) {
		    print_helperr(ERR_SET);
		    goto exit;
		}
	i=0;	
            for (c = 0; c < 4; c++)
                if (m_wepkey[c]) {
                    varbinds[i].oid = defaultWEPKey[c];
                    varbinds[i].len_oid = sizeof(defaultWEPKey[c]);
                    varbinds[i].type = STRING_VALUE;
                    varbinds[i].value = wepkey[c];
                    varbinds[i].len_val = 5;
                    i++;
             }
         if (snmp(varbinds, i, SET) <= 0) {
               print_helperr(ERR_SET);
               goto exit;
         }
		
		print_help(DONE_SET);
	    goto exit;
	}
	continue;
    }

    print_help(ANY_KEY);
  exit:
    getch();
  quit:
    print_top(NULL, NULL);
    clear_main(0);
}
